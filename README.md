# Enlaces recomendables y complementarios para ver en estos videos de XCode y Gitlab

[Introducción a Git con Xcode de EfectoApple.com (turorial1)](https://www.efectoapple.com/tutorial-introduccion-git-xcode-1)

[Introducción a Git con Xcode de EfectoApple.com (turorial2)](https://www.efectoapple.com/tutorial-introduccion-git-xcode-2)

[Introducción a Git con Xcode de EfectoApple.com (turorial3)](https://www.efectoapple.com/tutorial-introduccion-git-xcode-3)


**para poder modificar el nombre del ultimo commit** `git --amend `

**Para poder eliminar hasta una linea de tiempo todos los commits** `git reset --hard HAS`